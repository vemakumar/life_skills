# NoSql Databases and uses
## Introduction :
If you’re looking to explore the world of database management, you’ve probably heard of NoSQL databases. Unlike traditional SQL databases, NoSQL databases offer a flexible, scalable approach to data storage and retrieval that’s particularly suited to modern web applications.


## What is NoSQL Database?

NoSQL (Not Only SQL) databases are non-relational databases that do not use the traditional tabular relations used by SQL databases. Unlike SQL databases that use structured query language, NoSQL databases use different query languages and data models, such as key-value, document, column-family, and graph.

Examples of popular NoSQL databases include MongoDB, Cassandra, Couchbase, Redis, and Amazon DynamoDB.


## What are the Types Of NoSQL Databases?
There are several types of NoSQL databases, each with its own unique data model and query language. The main types of NoSQL databases are:

![img](https://miro.medium.com/v2/resize:fit:720/format:webp/1*IThqoYJMcgO0Da7YtRZKIw.jpeg)

1. Key-Value Stores: These databases store information in pairs, like a dictionary. They're really good at handling lots of data quickly. Examples are Redis, Riak, and Amazon DynamoDB.

2. Document Databases: These databases store data in a flexible, document-like format, such as JSON. They're great for storing data that doesn't fit neatly into tables. Examples include MongoDB, Couchbase, and Apache CouchDB.

3. Column-family Stores: Instead of organizing data in rows and columns like a spreadsheet, these databases organize it by columns. They're designed to handle large amounts of data efficiently. Examples are Apache Cassandra and HBase.

4. Graph Databases: These databases use a structure of nodes and connections to represent and store data. They're really good for handling complex relationships between different pieces of information. Examples include Neo4j, OrientDB, and Amazon Neptune.

5. Object Databases: These databases store objects directly, which is useful for programming languages that use objects heavily. They're good for object-oriented programming. Examples include db4o and Objectivity/DB.



## What are the benefits of using a NoSQL Database?

1. NoSQL databases offer flexibility, scalability, speed, reliability, and distributed architecture.
They feature schema-free design, easy replication, and simple APIs.
2. NoSQL databases excel in performance compared to SQL databases, especially for scenarios where data aligns with code objects, minimizing complex joins.
3. Their flexible schemas facilitate rapid adjustments to changing requirements, enabling iterative development and faster feature delivery.
4. Horizontal scaling is possible with NoSQL databases through the addition of inexpensive servers, unlike the costly vertical scaling required by SQL databases.
5. Data storage formats in NoSQL databases are optimized for efficient queries, outperforming normalized data in SQL databases.
6. Examples like MongoDB provide seamless integration with popular programming languages, reducing code complexity, development time, and potential errors.

# When to use a NoSQL Database?
In the late 1990s, almost every type of applications were based on SQL type databases. But the rise of Internet has changed everything in the context of application development. These changes have influenced many organizations of all sizes to adopt a new database technology called NoSQL. Below are some of the facts when we should consider using a NoSQL databases.

1. If you want a faster development
2. If you want structured and semi-structured data storage
3. If you have a requirement of scale-out architecture
4. If you want to handle huge amount of data
5. If your application is using microservices and real-time streaming.

## References :

1. Intro : https://www.geeksforgeeks.org/introduction-to-nosql/ 
2. NoSql : https://www.mongodb.com/nosql-explained
3. NoSQL databases : https://javatechonline.medium.com/types-of-nosql-databases-and-their-features-bda05d9a361f
4. Benefits : https://javatechonline.com/types-of-nosql-databases-and-examples/#What_are_the_benefits_of_using_a_NoSQL_Database